import { sum, validate } from '../lib/calculator.js'
import handleSum from '../pages/api/sum'
import { createMocks } from 'node-mocks-http'
import networth from '../lib/constants.js'
import _ from 'lodash'

describe('/calculator/', () => {
  test('Sum: adds -0.9 + 9.87765 + 2354353234234 to equal 2,354,353,234,242.97', () => {
    expect(sum({ 
      a: -0.9, b: 9.87765,
      c:2354353234234 
    })).toBe(2354353234242.98);
  });

  test('Validate: error if it is NaN "sdfsdf"', () => {
    expect(_.isEmpty(validate({ 
      a: "sdfsdf",
      b: 9.87765,
      c:2.66 
    }))).toBe(false);
  });

  test('Validate: error if it is Number -0.9', () => {
    expect(_.isEmpty(validate({ a: '-0.9', b: 9.87765, c:2.66 }))).toBe(true);
  });
})

describe('/api/sum', () => {
  test('returns 200', async () => {
    const { req, res } = createMocks({
      method: 'POST',
      url: '/api/sum',
      headers: {'Content-Type': 'application/json'},
      body: networth,
    });
   
    await handleSum(req, res);

    expect(res._getStatusCode()).toBe(200);
  });

  test('returns totals keys', async () => {
    const { req, res } = createMocks({
      method: 'POST',
      url: '/api/sum',
      headers: {'Content-Type': 'application/json'},
      body: networth,
    });
   
    await handleSum(req, res);

    expect(JSON.parse(res._getData())).toHaveProperty('totalAssets')
    expect(JSON.parse(res._getData())).toHaveProperty('totalLiabilities')
  });

  test('verify totals are type number', async () => {
    const { req, res } = createMocks({
      method: 'POST',
      url: '/api/sum',
      headers: {'Content-Type': 'application/json'},
      body: networth,
    });
   
    await handleSum(req, res);

    expect(typeof JSON.parse(res._getData()).totalAssets).toBe('number')
  });

  test('returns error when empty data', async () => {
    const { req, res } = createMocks({
      method: 'POST',
      url: '/api/sum',
      headers: {'Content-Type': 'application/json'},
      body: {},
    });
   
    await handleSum(req, res);
    
    expect(JSON.parse(res._getData())).toHaveProperty('error')
  });


});

//proj goalrole tech
//modernize kpi=key performance indicators=  ammounts of cliskc to do stuff, time to load, mobile usage 
