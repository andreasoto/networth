import React, { useState, useEffect } from 'react'
import {Table,TableBody, TableContainer, TableHead, TableRow, TableCell, FormControl , InputLabel , Select, MenuItem } from '@material-ui/core'
import CurrencyTextField from '@unicef/material-ui-currency-textfield'
import {getExchangeData, getExchangeRate} from '../lib/calculator'
import { makeStyles } from '@material-ui/core/styles'
import defaultNetWorth from '../lib/constants.js'

const Page = () => { 
    const [networth, setNetworth] = useState(defaultNetWorth)
    const [totals, setTotal] = useState({totalAssets: 0.00, totalLiabilities: 0.00})
    const [currency, setCurrency] = useState({ value: 'CAD', symbol: '$'})
    const [input, setInput] = useState({ value: 0.00, id: 'none', isAsset: false})
    let timeOutId

    //Form styles
    const useStyles = makeStyles((theme) => ({
        formControl: {
            margin: theme.spacing(1),
            minWidth: 120,
        },
        selectEmpty: {
        marginTop: theme.spacing(2),
        },
        table: {
            maxWidth: 700
        },
        hideBorder:{
            borderBottom : 0
        }
    }))
    
    const classes = useStyles()

    const updateTotals = async () => {
        const res = await fetch(`/api/sum`, {
            method: "POST",
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(networth)} )
                .then(response => response.json())
                .then(totals => { setTotal(totals)})
    }

    const updateCurrency = async (e) => {
        const rate = await getExchangeRate(currency.value, e.target.value)
        let assets = getExchangeData(rate, networth.assets)
        let liabilities = getExchangeData(rate, networth.liabilities)
        setCurrency({
            value: e.target.value,
            symbol: '$'
            }
        )
        setNetworth({
            assets,
            liabilities
            }
        )
    }

    const handleOnChange = (e, value, isAsset) =>{
        setInput({value: value, id: e.target.id, isAsset: isAsset})
    }
    const addToNetworth = () => {
        if (input.isAsset) {
            setNetworth({ 
                ...networth, 
                assets: {
                    ...networth.assets, 
                    [input.id]: (input.value)
                }
            })
        } else {
            setNetworth({
                ...networth,
                liabilities: {
                    ...networth.liabilities,
                    [input.id]: (input.value)
                }
            })
        }      
    }

    useEffect(updateTotals, [networth])
    
    useEffect(() => {
        timeOutId = setTimeout(addToNetworth, 500)
        return () => clearTimeout(timeOutId)
        
    }, [input])

     return <div>
        
        <TableContainer >
        <Table aria-label="simple table" className={classes.table}>
            <TableHead>
            <TableRow>
                <TableCell>
                    <h2>Tracking your Networth</h2>
                </TableCell>
                <TableCell align="right">
                <FormControl className={classes.formControl}>
                    <InputLabel id="demo-simple-select-label">Select Currency</InputLabel>
                    <Select value={currency.value}
                        onChange={updateCurrency}
                        displayEmpty
                        inputProps={{ 'aria-label': 'Without label' }}>
                    <MenuItem value={'CAD'}>CAD</MenuItem>
                    <MenuItem value={'USD'}>USD</MenuItem>
                    <MenuItem value={'CLP'}>CLP </MenuItem>
                    </Select>
                </FormControl>
                </TableCell>
            </TableRow>
            <TableRow>
                <TableCell>Net Worth</TableCell>
                <TableCell align="right">
                    <CurrencyTextField id="networth" readonly disabled
                        value={ parseFloat(totals.totalAssets - totals.totalLiabilities)} />
                </TableCell>
            </TableRow>
            <TableRow>
                <TableCell colSpan={2}>Assets</TableCell>
            </TableRow>
            <TableRow>
                <TableCell colSpan={2}>Cash and Investments</TableCell>
            </TableRow>
            </TableHead>
            <TableBody>
                <TableRow >
                <TableCell component="th" scope="row"  className={classes.hideBorder} >Chequing</TableCell>
                <TableCell align="right" className={classes.hideBorder}>
                    <CurrencyTextField id="chequing"
                        onChange={(e, value) => handleOnChange(e, value, true)}
                        value={networth.assets.chequing} /> 
                </TableCell>
                </TableRow>
                <TableRow >
                    <TableCell component="th" scope="row" className={classes.hideBorder}>Savings for Taxes</TableCell>
                    <TableCell align="right" className={classes.hideBorder}> 
                        <CurrencyTextField id="savTaxes"
                            onChange={(e, value) => handleOnChange(e, value, true)}
                            value={networth.assets.savTaxes} />
                    </TableCell>
                </TableRow>
                <TableRow >
                    <TableCell component="th" scope="row" className={classes.hideBorder}>Rainy Days</TableCell>
                    <TableCell align="right" className={classes.hideBorder}>
                        <CurrencyTextField id="rainyDay"
                            onChange={(e, value) => handleOnChange(e, value, true)}
                            value={networth.assets.rainyDay} />
                    </TableCell>
                </TableRow>
                <TableRow >
                    <TableCell component="th" scope="row" className={classes.hideBorder}>Savings for Fun</TableCell>
                    <TableCell align="right" className={classes.hideBorder}>
                        <CurrencyTextField id="savFun"
                            onChange={(e, value) => handleOnChange(e, value, true)}
                            value={networth.assets.savFun} />
                    </TableCell>
                </TableRow>
                <TableRow >
                    <TableCell component="th" scope="row" className={classes.hideBorder}>Savings for Travel</TableCell>
                    <TableCell align="right" className={classes.hideBorder}>
                        <CurrencyTextField id="savTravel"
                            onChange={(e, value) => handleOnChange(e, value, true)}
                            value={networth.assets.savTravel} />
                    </TableCell>
                </TableRow>
                <TableRow >
                    <TableCell component="th" scope="row" className={classes.hideBorder}>Savings for Personal</TableCell>
                    <TableCell align="right" className={classes.hideBorder}>
                        <CurrencyTextField id="savPersonal"
                        onChange={(e, value) => handleOnChange(e, value, true)}
                        value={networth.assets.savPersonal} />
                    </TableCell>
                </TableRow>
                <TableRow >
                    <TableCell component="th" scope="row" className={classes.hideBorder}>Investment 1</TableCell>
                    <TableCell align="right" className={classes.hideBorder}>
                        <CurrencyTextField id="invest1"
                        onChange={(e, value) => handleOnChange(e, value, true)}
                        value={networth.assets.invest1} />
                    </TableCell>
                </TableRow>
                <TableRow >
                    <TableCell component="th" scope="row" className={classes.hideBorder}>Investment 2</TableCell>
                    <TableCell align="right" className={classes.hideBorder}>
                        <CurrencyTextField id="invest2"
                        onChange={(e, value) => handleOnChange(e, value, true)}
                        value={networth.assets.invest2} />
                    </TableCell>
                </TableRow>
                <TableRow >
                    <TableCell component="th" scope="row" className={classes.hideBorder}>Investment 3</TableCell>
                    <TableCell align="right" className={classes.hideBorder}>
                        <CurrencyTextField id="invest3"
                        onChange={(e, value) => handleOnChange(e, value, true)}
                        value={networth.assets.invest3} />
                    </TableCell>
                </TableRow>
                <TableRow >
                    <TableCell component="th" scope="row" className={classes.hideBorder}>Investment 4</TableCell>
                    <TableCell align="right" className={classes.hideBorder}>
                        <CurrencyTextField id="invest4"
                        onChange={(e, value) => handleOnChange(e, value, true)}
                        value={networth.assets.invest4} />
                    </TableCell>
                </TableRow>
                <TableRow>
                    <TableCell colSpan={2}>Long Term Assets</TableCell>
                </TableRow>
                <TableRow >
                    <TableCell component="th" scope="row" className={classes.hideBorder}>Primary Home</TableCell>
                    <TableCell align="right" className={classes.hideBorder}>
                        <CurrencyTextField id="primHome"
                        onChange={(e, value) => handleOnChange(e, value, true)}
                        value={networth.assets.primHome} />
                    </TableCell>
                </TableRow>
                <TableRow >
                    <TableCell component="th" scope="row" className={classes.hideBorder}>Second Home</TableCell>
                    <TableCell align="right" className={classes.hideBorder}>
                        <CurrencyTextField id="seconHome"
                        onChange={(e, value) => handleOnChange(e, value, true)}
                        value={networth.assets.seconHome} />
                    </TableCell>
                </TableRow>
                <TableRow >
                    <TableCell component="th" scope="row" className={classes.hideBorder}>Other</TableCell>
                    <TableCell align="right" className={classes.hideBorder}>
                        <CurrencyTextField id="other"
                        onChange={(e, value) => handleOnChange(e, value, true)}
                        value={networth.assets.other} />
                    </TableCell>
                </TableRow>
                <TableRow>
                    <TableCell>Total Assets</TableCell>
                    <TableCell align="right">
                        <CurrencyTextField id="totalAssets" readonly disabled
                        value={totals.totalAssets}
                        currencySymbol={currency.symbol} />
                    </TableCell>
                </TableRow>
            </TableBody>
        </Table>
        <Table aria-label="simple table" className={classes.table}>
            <TableHead>
                <TableRow>
                    <TableCell colSpan={3}>Liabilities</TableCell>
                </TableRow>
                <TableRow>
                    <TableCell>Short Term Liabilities</TableCell>
                    <TableCell align="right">Monthly Payment</TableCell>
                    <TableCell align="right"></TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                <TableRow >
                    <TableCell component="th" scope="row" className={classes.hideBorder}>Credit Card 1</TableCell>
                    <TableCell align="right" className={classes.hideBorder}>
                        <CurrencyTextField  />
                    </TableCell>
                    <TableCell align="right" className={classes.hideBorder}>
                        <CurrencyTextField id="cc1"
                        onChange={(e, value) => handleOnChange(e, value, false)}
                        value={networth.liabilities.cc1} />
                    </TableCell>
                </TableRow>
                <TableRow >
                    <TableCell component="th" scope="row" className={classes.hideBorder}>Credit Card 2</TableCell>
                    <TableCell align="right" className={classes.hideBorder}>
                        <CurrencyTextField />
                        </TableCell>
                    <TableCell align="right" className={classes.hideBorder}>
                        <CurrencyTextField id="cc2"
                        onChange={(e, value) => handleOnChange(e, value, false)}
                        value={networth.liabilities.cc2} />
                    </TableCell>
                </TableRow>
                <TableRow >
                    <TableCell component="th" scope="row" >Long Term Liabilities</TableCell>
                    <TableCell colSpan={2}></TableCell>
                </TableRow>
                <TableRow >
                    <TableCell component="th" scope="row" className={classes.hideBorder}>Mortage 1</TableCell>
                    <TableCell align="right" className={classes.hideBorder}>
                        <CurrencyTextField />  </TableCell>
                    <TableCell align="right" className={classes.hideBorder}>
                        <CurrencyTextField id="mort1"
                        onChange={(e, value) => handleOnChange(e, value, false)}
                        value={networth.liabilities.mort1} />
                    </TableCell>
                </TableRow>
                <TableRow >
                    <TableCell component="th" scope="row" className={classes.hideBorder}>Mortgage 2</TableCell>
                    <TableCell align="right" className={classes.hideBorder}>
                        <CurrencyTextField />
                    </TableCell>
                    <TableCell align="right" className={classes.hideBorder}>
                        <CurrencyTextField id="mort2"
                        onChange={(e, value) => handleOnChange(e, value, false)}
                        value={networth.liabilities.mort2} />
                    </TableCell>
                </TableRow>
                <TableRow >
                    <TableCell component="th" scope="row" className={classes.hideBorder}>Line of Credit</TableCell>
                    <TableCell align="right" className={classes.hideBorder}>
                        <CurrencyTextField />
                    </TableCell>
                    <TableCell align="right" className={classes.hideBorder}>
                        <CurrencyTextField id="lineCredit"
                        onChange={(e, value) => handleOnChange(e, value, false)}
                        value={networth.liabilities.lineCredit} />
                    </TableCell>
                </TableRow>
                <TableRow >
                    <TableCell component="th" scope="row" className={classes.hideBorder}>Investment Loan</TableCell>
                    <TableCell align="right" className={classes.hideBorder}>
                        <CurrencyTextField />
                    </TableCell>
                    <TableCell align="right" className={classes.hideBorder}>
                        <CurrencyTextField id="investLoan"
                        onChange={(e, value) => handleOnChange(e, value, false)}
                        value={networth.liabilities.investLoan} />
                    </TableCell>
                </TableRow>
                <TableRow>
                    <TableCell colSpan={2}>Total Liabilities</TableCell>
                    <TableCell align="right">
                        <CurrencyTextField id="totalLiabilities" readonly disabled
                        value={totals.totalLiabilities}
                        currencySymbol={currency.symbol} />
                    </TableCell>
                </TableRow>
            </TableBody>
        </Table>
        </TableContainer>
</div>}
export default Page 
