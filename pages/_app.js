import Page from '../pages/index'
import Head from 'next/head'

export default function Application({ Component, pageProps }){

    return (  
        <div>
        <Head>
            <title>Net Worth</title>
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" /></Head>
        <Page {...pageProps} />
        </div>
    )
}
