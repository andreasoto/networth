import nc from 'next-connect'
import { getCurrency } from '../../lib/calculator'
import _ from 'lodash'


export default async (req, res) => {
  if (req.method === 'POST') {
    if (_.isEmpty(req.body)) {
        return res.json({
          error: "error: empty"
      })
    }
    let exchangeRate = await getCurrency(req.body.from,req.body.to)
    if (_.isNaN(exchangeRate)) {
      return res.json({
          error: "invalid exchange rate"
      })
    }
    res.json({ exchangeRate: exchangeRate })
  } 
}