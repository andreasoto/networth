import nc from 'next-connect'
import { sum, validate } from '../../lib/calculator'
import _ from 'lodash'

export default (req, res) => {
  if (req.method === 'POST') {
      if (_.isEmpty(req.body)) {
        return res.json({
          error: "error: empty"
      })
    }
    if (!_.isEmpty(validate(req.body.assets)) || !_.isEmpty(validate(req.body.liabilities) ) ) {
            return res.json({
                error: "error - invalid fields"
            })
          }
          res.statusCode = 200;
          res.setHeader('Content-Type', 'application/json');
          res.json({
                  totalAssets: sum(req.body.assets),
                  totalLiabilities: sum(req.body.liabilities)
              })
  }
}