export default {
    assets: {
        chequing: 1.00,
        savTaxes: 1.00,
        rainyDay: 1.00,
        savFun: 1.00,
        savTravel: 1.00,
        savPersonal: 1.00,
        invest1: 1.00,
        invest2: 1.00,
        invest3: 1.00,
        invest4: 1.00,
        primHome: 1.00,
        seconHome: 1.00,
        other: 0.00 
    },
    liabilities: {
        cc1 : 1.00,
        cc2: 1.00,
        mort1: 1.00,
        mort2: 1.00,
        lineCredit: 1.00,
        investLoan: 1.00
    }
};