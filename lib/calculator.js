const sum = ( data ) => {
    let total = 0.00;
    for ( const [key,value] of Object.entries(data)){
        total += parseFloat(value)
    }
    return parseFloat(total.toFixed(2))
};

const validate = ( data ) => {
    let error = []
    for ( const [key,value] of Object.entries(data)){
        if( isNaN( parseFloat(value) )){
            error.push(key)
        }
    }
    return error
}

const getCurrency = async (from, to) => {
    const response = await fetch(`https://free.currconv.com/api/v7/convert?q=${from}_${to}&compact=ultra&apiKey=${process.env.CURRENCYAPIKEY}`)
    const data = await response.json()
    return Object.values(data)[0]
}

const getExchangeData = (rate, data) => {
    let results = {}
    for (const [key,value] of Object.entries(data)){
        results[key] = value * rate
        results[key] = results[key].toFixed(2) 
    }
    return results
}

const getExchangeRate = async (fromCurrency, toCurrency) => {
    const response = await fetch(`/api/currency/`, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ from: fromCurrency, to: toCurrency})
    });
    const data = await response.json()
    return data.exchangeRate
  };

export { 
    sum,
    validate,
    getCurrency,
    getExchangeData,
    getExchangeRate
}